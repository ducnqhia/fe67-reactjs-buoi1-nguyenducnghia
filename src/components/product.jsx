import React from "react";

class Product extends React.Component {
  render() {
    let {sanPham} = this.props;
    return (
        <div className="col-xs-12 col-sm-6 col-md-6 col-lg-3 col-lg-3 px-2">
          {/* <div className="container"> */}
            <div className="card bg-light">
              <img
                className="card-img-top"
                src={sanPham.hinhAnh}
                alt="Card image"
                style={{ maxWidth: "100%", height: 250 }}
              />
              <div className="card-body text-center">
                <h4 className="card-title text-center">{sanPham.tenSP}</h4>
                <p className="card-text">{sanPham.thongtinSP}</p>
                <a href="#" className="btn btn-primary mr-1">
                  Detail
                </a>
                <a href="#" className="btn btn-danger">
                  Cart
                </a>
              </div>
            </div>
          {/* </div> */}
        </div>
    );
  }
}
export default Product;
