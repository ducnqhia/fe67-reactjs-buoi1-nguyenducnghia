import React from 'react'
import './slide.css'

class Slide extends React.Component{
    render(){
        return (
          <section
            id="slide"
            className="carousel slide"
            data-ride="carousel"
          >
            <ol className="carousel-indicators">
              <li
                data-target="#slide"
                data-slide-to={0}
                className="active"
              />
              <li data-target="#slide" data-slide-to={1} />
              <li data-target="#slide" data-slide-to={2} />
            </ol>
            <div className="carousel-inner">
              <div className="carousel-item active">
                <img src="./img/slide_1.jpg" className="d-block w-100" alt="..." />
              </div>
              <div className="carousel-item">
                <img src="./img/slide_2.jpg" className="d-block w-100" alt="..." />
              </div>
              <div className="carousel-item">
                <img src="./img/slide_3.jpg" className="d-block w-100" alt="..." />
              </div>
            </div>
            <a
              className="carousel-control-prev"
              href="#slide"
              role="button"
              data-slide="prev"
            >
              <span className="carousel-control-prev-icon" aria-hidden="true" />
              <span className="sr-only">Previous</span>
            </a>
            <a
              className="carousel-control-next"
              href="#slide"
              role="button"
              data-slide="next"
            >
              <span className="carousel-control-next-icon" aria-hidden="true" />
              <span className="sr-only">Next</span>
            </a>
          </section>
        );
    }
}
export default Slide 