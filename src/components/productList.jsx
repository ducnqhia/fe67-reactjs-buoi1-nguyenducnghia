import React from "react";
import Product from "./product";
import './productList.css'

class ProductList extends React.Component {
    renderTitle = () => {
        let { mangsanpham } = this.props;
        let index = mangsanpham.findIndex(sp =>  sp.loaiSP === 'laptop');
        if (index === -1)
        {
            return <h1 className="text-center text-white">BEST SMARTPHONE</h1>
        }
        return <h1 className="text-center text-white">BEST LAPTOP</h1>
      };
    renderSanPham = () => {
        let { mangsanpham } = this.props;
        return mangsanpham.map((sp, index) => {
        return <Product sanPham={sp} key={index} />;
        });
    };
  render() {
    return (
      <section className="product bg-dark">
        <div className="container-fluid py-5 px-4">
          {this.renderTitle()}
          <div className="row mx-0">{this.renderSanPham()}</div>
        </div>
      </section>
    );
  }
}
export default ProductList;
