import React from 'react';
import Header from './header'
import Slide from './slide';
import ProductList from './productList'
import Footer from './footer';

export default class Main extends React.Component{
     mangSP  =[
         { maSP: 1, loaiSP:'smartphone', tenSP: 'Black Berry',hinhAnh:'./img/sp_blackberry.png', thongtinSP:'iPhone X features a new all-screen design. Face ID, which makes your face your password', gia: 1000 },
         { maSP: 2, loaiSP:'smartphone', tenSP: 'Iphone X',hinhAnh:'./img/sp_iphoneX.png', thongtinSP:'The Galaxy Note7 comes with a perfectly symmetrical design for good reason', gia: 2000 },
         { maSP: 3, loaiSP:'smartphone', tenSP: 'Note 7',hinhAnh:'./img/sp_note7.png',  thongtinSP:'A young global smartphone brand focusing on introducing perfect sound quality',gia: 3000 },
         { maSP: 4, loaiSP:'smartphone', tenSP: 'Vivo 850',hinhAnh:'./img/sp_vivo850.png', thongtinSP:'BlackBerry is a line of smartphones, tablets, and services originally designed', gia: 3000 },
         { maSP: 5, loaiSP:'laptop', tenSP: 'MACBOOK',hinhAnh:'./img/lt_macbook.png', thongtinSP:'The MacBook is a brand of notebook computers manufactured by Apple Inc', gia: 1000 },
         { maSP: 6, loaiSP:'laptop', tenSP: 'ASUS ROG',hinhAnh:'./img/lt_rog.png', thongtinSP:'The Asus ROG Strix Flare is the latest addition to Asus lineup of ROG branded devices', gia: 2000 },
         { maSP: 7, loaiSP:'laptop', tenSP: 'HP OMEN',hinhAnh:'./img/lt_hp.png',  thongtinSP:'A young global smartphone brand focusing on introducing perfect sound quality',gia: 3000 },
         { maSP: 8, loaiSP:'laptop', tenSP: 'LENOVO THINKPAD',hinhAnh:'./img/lt_lenovo.png', thongtinSP:'The ThinkPad X1 Carbon is a high-end notebook computer released by Lenovo in 2012', gia: 3000 },
      ]
      mangDT = this.mangSP.filter(sp => sp.loaiSP === 'smartphone');   
      mangLapTop = this.mangSP.filter(sp => sp.loaiSP === 'laptop');  
    render() {
       return(
        <div>
            <Header/>
            <Slide/>
            <ProductList mangsanpham={this.mangDT}/>
            <ProductList mangsanpham={this.mangLapTop}/>
            <Footer/>
         </div>
       )
    }
}